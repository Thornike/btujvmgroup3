package package1;

public class Test1 {
    public  int x1 = 1;
    private int x2 = 2;
    int x3 = 3;
    protected int x4 = 4;

    @Override
    public String toString() {
        return "Test1{" +
                "x1=" + x1 +
                ", x2=" + x2 +
                ", x3=" + x3 +
                ", x4=" + x4 +
                '}';
    }
}
