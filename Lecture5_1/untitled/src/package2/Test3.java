package package2;

import package1.Test1;

public class Test3 extends Test1 {
    public void printFields(){
        System.out.println(x1);
        System.out.println(x4);
    }

    @Override
    public String toString() {
        return "Test3{" +
                "x1=" + x1 +
                ", x4=" + x4 +
                '}';
    }
}
